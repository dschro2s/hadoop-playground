import java.io.IOException;

import com.opencsv.CSVParser;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class GenderAnalysis {

    public static class TokenizerMapper
            extends Mapper<Object, Text, Text, IntWritable> {

        private Text gender = new Text();

        @Override
        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {
            CSVParser csv = new CSVParser();
            String[] row = csv.parseLine(value.toString());

            String genderStr = row[5].trim();

            if (genderStr.length() == 0) {
                gender.set(new Text("unknown"));
            } else {
                gender.set(genderStr);
            }

            context.write(gender, new IntWritable(Integer.parseInt(row[21])));
        }
    }

    public static class GenderReducer
            extends Reducer<Text, IntWritable, Text, IntWritable> {

        private IntWritable result = new IntWritable();

        public void reduce(Text key, Iterable<IntWritable> values,
                           Context context
        ) throws IOException, InterruptedException {
            int sum = 0;
            for (IntWritable val : values) {
                sum += val.get();
            }
            result.set(sum);
            context.write(key, result);

        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "retweets per gender");
        job.setJarByClass(GenderAnalysis.class);
        job.setMapperClass(TokenizerMapper.class);
        job.setCombinerClass(GenderReducer.class);
        job.setReducerClass(GenderReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
